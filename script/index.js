(function() {
  var app;

  app = angular.module('myApp', ['gridster']);

  app.controller('myCtrl', function($scope, $http) {
    $scope.sections = [];
    $scope.getHeaderValues = function() {
      angular.forEach($scope.sections, (function(value, key) {
        if (value.Name === "Header") {
          return $scope.headerbg = value.Value.BgColor;
        }
      }));
    };
    $scope.addOverview = function() {};
    $scope.addTeam = function() {};
    $scope.addJobs = function() {};
    $scope.addFooter = function() {};
    $http.get('json/sample.js').success(function(response) {
      angular.forEach(response, (function(value, key) {
        return $scope.sections.push(value);
      }));
    });
    $scope.addSectionShow = function() {
      $scope.showAddSection = true;
      $scope.overlay = true;
    };
    $scope.addSectionHide = function() {
      $scope.showAddSection = false;
      $scope.overlay = false;
    };
    $scope.showTeamEditor = function() {
      $scope.showTeamEditor = true;
    };
  });

}).call(this);
