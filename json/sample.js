[ {
  "Name": "Overview",
  "Value": {
    "BgColor": "",
    "Header": {
      "Value": "",
      "Font": {
        "Size": "",
        "Style": "",
        "Color": "",
        "Weight": ""
      }
    },
    "Text": {
      "Value": "",
      "Font": {
        "Size": "",
        "Style": "",
        "Color": "",
        "Weight": ""
      }
    },
    "Button": {
      "Value": "",
      "Font": "",
      "Style": ""
    }
  }
}, {
  "Name": "Header",
  "Value": {
    "BgColor": "#eaeaea",
    "LogoFlag": "",
    "Name": {
      "Flag": "",
      "Font": {
        "Size": "",
        "Style": "",
        "Color": "",
        "Weight": ""
      }
    }
  }
},{
  "Name": "Team",
  "Value": {
    "BgColor": "",
    "Members": [{
      "ImageDataUrl": "profile-pics/2_1445854541307_stevie.jpg",
      "Fullname": "Nikh Boh",
      "Title": "Senior Analyst",
      "Account": "2",
      "Firstname": "Nikh",
      "Lastname": " Boh",
      "$$hashKey": "object:27"
    }]
  }
}, {
  "Name": "Jobs",
  "Value": {
    "BgColor": "",
    "Type": "",
    "Flat": ["56", "57", "58", "59"],
    "Grouped": [{
      "Department": "Engineering",
      "JobList": ["56"]
    }, {
      "Department": "Sales",
      "JobList": ["57", "59"]
    }, {
      "Department": "Finance",
      "JobList": ["58"]
    }]
  }
}, {
  "Name": "Footer",
  "Value": {
    "BgColor": ""
  }
}]
